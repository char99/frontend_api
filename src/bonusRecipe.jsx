import axios from "axios";
import React from "react";

class BonusRecipe extends React.Component {
  constructor() {
    super();
    this.state = {
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    event.preventDefault()
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    let tempObj = this.state;
    tempObj.directions = [];
    let temp = this.state.direction.split("\n") || '';
    for (let i = 0; i < temp.length; i++) {
      tempObj.directions[i] = { instructions: temp[i] };
    }
    if (this.props.AllProps.reqType === "post") {
      this.props.AllProps.submitRecipePost(tempObj);
    } else {
      this.props.AllProps.submitRecipePatch(tempObj);
    }
  }

  
  render() {
    return (
      <div>
        <label>Recipes</label>
        <form className="bonusForm" onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="title"
            placeholder="title"
            onChange={this.handleChange}
          ></input>
          <input
            type="text"
            name="description"
            placeholder="description"
            onChange={this.handleChange}
          ></input>
          <input
            type="text"
            name="servings"
            placeholder="servings"
            onChange={this.handleChange}
          ></input>
          <input
            type="text"
            name="prepTime"
            placeholder="preptime"
            onChange={this.handleChange}
          ></input>
          <input
            type="text"
            name="cookTime"
            placeholder="cooktime"
            onChange={this.handleChange}
          ></input>
          <textarea
            type="text"
            rows="6"
            name="direction"
            placeholder="direction"
            onChange={this.handleChange}
          ></textarea>

          <input className="btn" name="recipes" type="submit" value="Submit" />
        </form>
        <ul>
          <li
            onClick={() => {
              this.props.AllProps.handleFormClick("post");
            }}
          >
            select post
          </li>
          <li
            onClick={() => {
              this.props.AllProps.handleFormClick("patch");
            }}
          >
            select patch(currently selected recipe)
          </li>
        </ul>
      </div>
    );
  }
}

export default BonusRecipe;
