import React from "react";
import ReactDOM from "react-dom";
import List from "./list";
import Bonus from "./bonus";
class App extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <List />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
