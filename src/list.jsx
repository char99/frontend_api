import React from "react";
import axios from "axios";
import Details from "./details";
import Recipes from "./recipes";
import Bonus from "./bonus";
let sourceRecipes = axios.CancelToken.source();
let sourceSpecials = axios.CancelToken.source();

class List extends React.Component {
  constructor() {
    super();
    this.state = {
      recipes: [],
      specials: [],
      detailedView: false,
      recipeNumber: undefined,
      reqType: "post",
    };
    this.handleClick = this.handleClick.bind(this);
    this.getRecipes = this.getRecipes.bind(this);
    this.getSpecials = this.getSpecials.bind(this);
    this.handleFormClick = this.handleFormClick.bind(this);
    this.submitRecipePost = this.submitRecipePost.bind(this);
    this.submitRecipePatch = this.submitRecipePatch.bind(this);
    this.submitSpecialPost = this.submitSpecialPost.bind(this);
    this.submitSpecialPatch = this.submitSpecialPatch.bind(this);
  }

  submitSpecialPost(special) {
    axios
      .post("http://localhost:3001/specials", special)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  submitSpecialPatch(special, ep) {
    axios
      .patch("http://localhost:3001/specials/" + ep, special)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  submitRecipePost(recipe) {
    recipe.postDate = new Date();
    console.log("post");
    axios
      .post("http://localhost:3001/recipes", recipe)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  submitRecipePatch(recipe) {
    recipe.editDate = new Date();
    if (!this.state.recipeNumber) {
      return console.log('click on a recipe first')
    }
    else {axios
      .patch(
        "http://localhost:3001/recipes/" +
          this.state.recipes[this.state.recipeNumber].uuid,
        recipe
      )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }
  handleFormClick(submitType) {
    console.log(submitType);
    this.setState({
      reqType: submitType,
    });
  }
  handleClick(event) {
    this.setState({
      detailedView: true,
      recipeNumber: Number(event.currentTarget.dataset.key),
    });
  }
  getSpecials() {
    axios
      .get("http://localhost:3001/specials", {
        cancelToken: sourceSpecials.token,
      })
      .then((res) => {
        this.setState({
          specials: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  getRecipes() {
    axios
      .get("http://localhost:3001/recipes", {
        cancelToken: sourceRecipes.token,
      })
      .then((res) => {
        this.setState({
          recipes: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  componentDidMount() {
    this.getRecipes();
    this.getSpecials();
  }

  componentWillUnmount() {
    if (sourceRecipes) {
      sourceRecipes.cancel("");
    }
    if (sourceSpecials) {
      sourceSpecials.cancel("");
    }
  }
  render() {
    return (
      <div>
        <div id="mainView">
          <Recipes
            recipes={this.state.recipes}
            handleClick={this.handleClick}
          />

          {this.state.detailedView ? (
            <Details
              recipeDetails={this.state.recipes[this.state.recipeNumber]}
              specialsList={this.state.specials}
            />
          ) : (
            ""
          )}
        </div>
        <Bonus
          specialsList={this.state.specials}
          getSpecialsList = {this.getSpecials}
          handleFormClick={this.handleFormClick}
          submitRecipePost={this.submitRecipePost}
          submitRecipePatch={this.submitRecipePatch}
          submitSpecialPost={this.submitSpecialPost}
          submitSpecialPatch={this.submitSpecialPatch}
          reqType={this.state.reqType}
        />
      </div>
    );
  }
}

export default List;
