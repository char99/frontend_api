import React from "react";
import Specials from "./specials";

class Ingredients extends React.Component {
  constructor() {
    super();
    this.ingredientsList = this.ingredientsList.bind(this);
    this.matchIngredients = this.matchIngredients.bind(this);
  }
  matchIngredients(ingredient, x) {
    for (let i = 0; i < this.props.specialsList.length; i++) {
      if (ingredient.uuid === this.props.specialsList[i].ingredientId) {
        return (
          <div key={"ingredient" + x}>
            <div className="ingredient">
              <p>{ingredient.name}</p>
            </div>
            <Specials specials={this.props.specialsList[i]} />
          </div>
        );
      }
    }
    return (
      <div key={"ingredient" + x}>
        <div className="ingredient">
          <p>{ingredient.name} </p>
        </div>
      </div>
    );
  }

  ingredientsList() {
    return this.props.ingredientsList.map((ingredient, x) => {
      return this.matchIngredients(ingredient, x);
    });
  }

  render() {
    if (!this.props.ingredientsList) {
      return <div id= "ingredientsView">blank</div>
    }
    return (
      <div id="ingredientsView">
        <h2>Ingredients</h2>
        {this.ingredientsList()}
      </div>
    );
  }
}

export default Ingredients;
