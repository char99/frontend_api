import React from "react";
import Ingredients from "./ingredients";
import Directions from "./directions";

class Details extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div id="detailsView">
        <div id="descriptionView">
          <div className="text">
            <h2>{this.props.recipeDetails.description}</h2>
          </div>
          <div className="text">
            <p>servings: {this.props.recipeDetails.servings}</p>
          </div>
          <div className="text">
            <p>prep time: {this.props.recipeDetails.prepTime}</p>
          </div>
          <div className="text">
            <p>cook time: {this.props.recipeDetails.cookTime}</p>
          </div>
          <div className="text">
            <p>posted: {this.props.recipeDetails.postDate}</p>
          </div>
          {this.props.recipeDetails.editDate ? <div className="text">
            <p>edited: {this.props.recipeDetails.editDate}</p>
          </div> : ""}
        </div>
        <Ingredients
          ingredientsList={this.props.recipeDetails.ingredients}
          specialsList={this.props.specialsList}
        />
        <Directions directions={this.props.recipeDetails.directions} />
      </div>
    );
  }
}

export default Details;
