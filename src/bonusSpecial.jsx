import axios from "axios";
import React from "react";

class BonusSpecial extends React.Component {
  constructor() {
    super()
    this.state ={
      value : ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getSpecials = this.getSpecials.bind(this);
  }

  
  getSpecials() {
    return <div>
      <select name = "sp" value ={this.state.value} onChange ={this.handleChange}>
      {this.props.AllProps.specialsList.map((item, i) => {
        return <option key = {"spList" + i} value={item.uuid}>{item.title}</option>;
      })}
    </select>
    </div>;
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log(this.state, "specials");
    if (this.props.AllProps.reqType === "post") {
      this.props.AllProps.submitSpecialPost(this.state);
    } else {
      this.props.AllProps.submitSpecialPatch(this.state, this.state.sp);
    }
  }

  componentDidMount(){
    this.props.AllProps.getSpecialsList
  }
  render() {
    return (
      <div>
        {this.props.AllProps.reqType === "patch" ? this.getSpecials() : ""}
        <label>Specials</label>
        <form className="bonusForm" onSubmit={this.handleSubmit}>
          <input
            type="text"
            name="title"
            placeholder="title"
            onChange={this.handleChange}
          ></input>
          <input
            type="text"
            name="type"
            placeholder="type"
            onChange={this.handleChange}
          ></input>
          <input
            type="text"
            name="text"
            placeholder="text"
            onChange={this.handleChange}
          ></input>

          <input className="btn" type="submit" value="Submit" />
        </form>{" "}
        <ul>
          <li
            onClick={() => {
              this.props.AllProps.handleFormClick("post");
            }}
          >
            post
          </li>
          <li
            onClick={() => {
              this.props.AllProps.handleFormClick("patch");
            }}
          >
            patch(currently selected )
          </li>
        </ul>
      </div>
    );
  }
}

export default BonusSpecial;
