import React from "react";
import BonusSpecial from "./bonusSpecial";
import BonusRecipe from "./bonusRecipe";

class Bonus extends React.Component {
  constructor() {
    super();
    this.state = {
      display: "recipe",
    };
    this.toggleForm = this.toggleForm.bind(this);
  }

  toggleForm(whichForm) {
    this.setState({
      display: whichForm,
    });
  }

  render() {
    return (
      <div id="bonusView">
        <ul>
          <li
            onClick={() => {
              this.toggleForm("recipe");
            }}
          >
            Recipe
          </li>
          <li
            onClick={() => {
              this.toggleForm("special");
            }}
          >
            Special
          </li>
        </ul>
        <div id="inputForm">
          {this.state.display === "recipe" ? (
            <BonusRecipe AllProps={this.props} />
          ) : (
            <BonusSpecial AllProps={this.props} />
          )}
        </div>
      </div>
    );
  }
}

export default Bonus;
