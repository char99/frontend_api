import React from "react";

class Directions extends React.Component {
  constructor() {
    super();
  }

  render() {
    if (!this.props.directions) {
      return <div id ="directionsView">blank</div>
    }
    return (
      <div id="directionsView">
        <h2>Directions</h2>
        {this.props.directions.map((item, i) => {
          return (
            <div key={"dir" + i}>
              <p>{item.instructions}</p>
              <p>{item.optional}</p>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Directions;
