import React from "react";

class Recipes extends React.Component {
  constructor() {
    super();
  }

  render() {
    if (!this.props.recipes) {
      return <div>blank</div>
    }
    return (
      <div id="recipesView">
        {this.props.recipes.map((item, i) => {
          return (
            <div
              className="recipe"
              data-key={i}
              key={i}
              onClick={this.props.handleClick}
            >
              <a href="#">
                <div className="thumb">
                  
                  {item.images ? <img src={"." + item.images.small}/> : <div className="noimg">no picture</div> } 
                                 </div>
                <div className="titleText">
                  <h1>{item.title}</h1>
                </div>
              </a>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Recipes;
