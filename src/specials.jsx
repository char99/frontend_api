import React from "react";

class Specials extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="specials" data-key = {this.props.specials.uuid}>
        <div className="text">
          <h2>{this.props.specials.title}</h2>
        </div>
        <div className="text">
          <p>{this.props.specials.text}</p>
        </div>
        <div className="text">
          <p>{this.props.specials.type}</p>
        </div>
        <div className="text">
          <h2>{this.props.specials.code}</h2>
        </div>
      </div>
    );
  }
}

export default Specials;
